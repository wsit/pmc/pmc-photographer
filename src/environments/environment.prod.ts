
const prodUrl = "https://tomcat.picturemeclubbing.com/pmc";
export const environment = {
  production: true,
  // URL of production API

  apiBaseUrl: prodUrl,
  apiUrl: prodUrl + '/auth/api',
  pictureUrl: prodUrl + '/common/',
  eventPhotoUrl: prodUrl + '/img/images/',
  publicApiBaseUrl: prodUrl + '/public/api',
  eventPhotoSharingUrl: prodUrl + '/img/social-sharing/',

};
